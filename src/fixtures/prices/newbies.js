const newbiePrices = [
  {
    time: '2 минуты',
    priceKids: 2375,
    priceAdult: 2500
  },
  {
    time: '4 минуты',
    priceKids: 2400,
    priceAdult: 2900
  },
  {
    time: '5 минут',
    priceKids: 2800,
    priceAdult: 3500
  },
  {
    time: '6 минут',
    priceKids: 4800,
    priceAdult: 6000
  },
  {
    time: '10 минут',
    priceKids: 5600,
    priceAdult: 7000
  },
  {
    time: '15 минут',
    priceKids: 8000,
    priceAdult: 10000
  },
  {
    time: '30 минут',
    priceKids: 18000,
    priceAdult: 18000
  },
  {
    time: '60 минут',
    priceKids: 30000,
    priceAdult: 30000
  }
]

export default newbiePrices
