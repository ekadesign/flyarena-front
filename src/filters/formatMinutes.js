import Vue from 'vue'
import moment from 'moment-timezone'
import store from '../store'

Vue.filter('formatMinutes', (value, filterFormat) => {
  const { zone, format } = store.state.app.time

  if (value) {
    return moment().add(value, 'minute').tz(zone).locale('ru').minutes()
  }

  return ''
})
